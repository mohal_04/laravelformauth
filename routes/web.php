<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('/users/freeusername', function (Illuminate\Http\Request $request) {
    return response()->json(['response' => 'This is post method']); 
}); */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::namespace('Auth')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
	Route::get('/users/freeusername', 'RegisterController@checkUsernameAvailable');
});

