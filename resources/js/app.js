
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
	data: {
		username: '',
		loading_image: '../images/giphy.gif',
		loading_image_class: 'hide-loading-image',
		username_is_available: '',
		username_error_message: ''
	},
	methods: {
		username_free() {
			app.loading_image_class = "show-loading-image"
			$.ajax({
				method: "GET",
				url: "/users/freeusername",
				data: { username: app.username }
			})
			.done(function(msg) {
				var data_type = typeof(msg)
				app.loading_image_class = "hide-loading-image"
				if (typeof(msg) != "undefined" && msg) {
					app.username_error_message = msg.resp_message
					app.username_is_available = msg.resp
				}
			})
		}
	}
});
